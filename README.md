# Very Short Code Voting: ProVerif Proofs

This contains the proofs for an internet voting system which uses short voting codes.

The proofs are in ProVerif: https://gitlab.inria.fr/bblanche/proverif, using the [improved_scope_lemma](https://gitlab.inria.fr/bblanche/proverif/-/tree/improved_scope_lemma) branch. At the time of writing, the most recent commit is [a7ff60b1](https://gitlab.inria.fr/bblanche/proverif/-/commit/a7ff60b14c88ebd78ac2b05e03b14b6411c277f3).


## Privacy

This proofs privacy against unbounded number of (attacker-controlled) voters. 

You may execute the proof with `make`. Please ensure `proverif` is in the `$PATH`.

Sanity checks:
- `sanity.pvl` (execute with `make sanity`) finds vote, verify, count and tally events are reachable
- Remove one of the choice statements -> attack found
- Publish secret key -> attack found by decrypting encryption
- Publish permutation -> attack found (although trivially by ProVerif overapproximation)


## Verifiability

This proofs E2E verifiability using the verifiability framework (Cheval et al., CSF 2023), with an unbounded number of voters and elections. 

You may execute the proof with `make`. Please ensure `proverif` is in the `$PATH`.

To run the full proof, you need to execute the following commands:
- `make prop1` which proofs `Lemma_Common`, `Lemma_Cell_BB` and `Lemma_Final_Tally_Voter` of the voting framework
- `make prop2` which assumes the `prop1` lemmas, and proofs various lemmas in preparation of the `Lemma_Tally_Decreasing` of the voting framework
- `make prop2` which assumes the `prop2` lemmas, and proofs `Lemma_Tally_Decreasing` of the voting framework
- `make query1` which assumes the `prop3` lemmas, and proofs the counted votes are correct (intuitively)
- `make query2` which assumes the `prop3` lemmas, and proofs all verified votes are counted

Sanity checks:
- `sanity.pvl` (execute with `make sanity`) finds many points in the protocol reachable
- Publish voting sheet -> attack found
- Publish signature key -> attack found

(* hash *)
type hash_t.

(* XOR *)
type xor_part_t.
type xor_t.
fun xor_combine(xor_part_t, xor_part_t):xor_t.
reduc forall a,b:xor_part_t; getL(xor_combine(a,b),b) = a. 
reduc forall a,b:xor_part_t; getR(xor_combine(a,b),a) = b. 
(* equation forall value1: xor_part_t, value2: xor_part_t; xor_combine(value1, value2) = xor_combine(value2, value1). *)
fun xor_hash(xor_t):hash_t. 

(* randomness *)
type randomness_t.
fun randomness_combine(randomness_t, randomness_t): randomness_t.

(* randomized signature *)
type signature_private_key_t.
type signature_public_key_t.
type signature_t.
type signature_message_t.
fun signature_get_public_key(signature_private_key_t): signature_public_key_t.
fun signature_sign(signature_message_t, signature_private_key_t, randomness_t): signature_t.
letfun signature_sign_randomized(message: signature_message_t, private_key: signature_private_key_t) = 
    new randomness[]: randomness_t; let signature = signature_sign(message, private_key, randomness) in signature.
fun signature_verify(signature_public_key_t, signature_t, signature_message_t): bool
    reduc forall message: signature_message_t, private_key: signature_private_key_t, randomness: randomness_t;
            signature_verify(signature_get_public_key(private_key), signature_sign(message,private_key,randomness), message) = true
        otherwise forall public_key: signature_public_key_t, signature: signature_t, message: signature_message_t;
            signature_verify(public_key, signature, message) = false.

(* randomized asymmetric encryption *)
type encryption_private_key_t.
type encryption_public_key_t.
type encryption_t.
type encryption_message_t.
fun encryption_get_public_key(encryption_private_key_t): encryption_public_key_t.
fun encryption_encrypt(encryption_message_t, encryption_public_key_t, randomness_t): encryption_t.
letfun encryption_encrypt_randomized(message: encryption_message_t, public_key: encryption_public_key_t) = 
    new randomness: randomness_t; let encryption = encryption_encrypt(message, public_key, randomness) in encryption.
fun encryption_decrypt(encryption_t, encryption_private_key_t): encryption_message_t 
    reduc forall message: encryption_message_t, private_key: encryption_private_key_t, randomness: randomness_t; 
        encryption_decrypt(encryption_encrypt(message,encryption_get_public_key(private_key),randomness), private_key) = message.

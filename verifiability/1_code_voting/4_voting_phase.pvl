(* -lib ../0_framework/gsverif_library.pvl *)
(* -lib ../0_framework/voting_framework.pvl *)
(* -lib 0_primitives.pvl *) 
(* -lib 0_proverif_utils.pvl *) 
(* -lib 1_shared.pvl *) 
(* -lib 2_permutation.pvl *)
(* -lib 3_setup_phase.pvl *)

fun voting_CCV_once_per_id(nat): once_label_t.
fun voting_CCC_once_per_id(nat): once_label_t.

event VotingPhase_ControlComponentCast_Terminates().
event VotingPhase_ControlComponentCast_Terminates2().

event VotingPhase_ControlComponentConfirm_Terminates().
event VotingPhase_ControlComponentConfirm_Terminates2().

event VotingPhase_AgreementProcedure_Terminates().
event VotingPhase_AgreementProcedure_Terminates2().

event CC_Receive_Vote(election_id,nat,vote_code_t,xor_part_t).
event CC_Receive_Confirm(election_id,nat,encryption_t,xor_t).

event LinkVotecodeEncryption(election_id,nat,vote_code_t, encryption_t).
let VotingPhase_ControlComponentCast(election:election_id) =
    (* receive vote & retrieve the corresponding secrets *)
    in(adversary, (id:nat, vote_code:vote_code_t)) [precise];

    get ControlComponent_VoteState(=election,=id,=vote_code,vote_verification_part) in

    (* store cast vote *)
    let () = unique(voting_CCV_once_per_id(id)) in
    get ControlComponent_VoteEncryptionState(=election,=id, =vote_code, vote_code_encryption) in
    insert ControlComponent_CastState(election, id, vote_code_encryption);
    event LinkVotecodeEncryption(election, id,vote_code, vote_code_encryption);

    (* sign cast vote & expect back signature *)
    let id_encryption_encoding = encoder_id_encryption_signature_message(election, id, vote_code_encryption) in
    let signature = signature_sign_randomized(id_encryption_encoding, signature_private_key) in
    out(adversary, (id, vote_code, signature));
    in(adversary, (=id, =vote_code, signature2: signature_t));
    if attacker_signature_verify(signature2, id_encryption_encoding) then
    (
        (* send out verification *)
        event CC_Receive_Vote(election, id, vote_code,vote_verification_part); 
        out(adversary, (id,vote_verification_part));
        event VotingPhase_ControlComponentCast_Terminates()
    )
    else
        event VotingPhase_ControlComponentCast_Terminates2().

event InsertedCLEANSED(election_id,ballot). 

let VotingPhase_ControlComponentConfirm(election:election_id) =
    (* receive vote & retrieve the corresponding secrets *)
    in(adversary, (id:nat, confirm_authentication:xor_t));

    (* authenticate confirmation *)
    get ControlComponent_AuthenticateState(=election,=id,confirm_authentication_hash,confirmation_verification_part) in
    if xor_hash(confirm_authentication) = confirm_authentication_hash then
    (
        (* store confirmed vote *)
        get ControlComponent_CastState(=election,=id, vote_encryption) in
        event CC_Receive_Confirm(election, id, vote_encryption, confirm_authentication); 
        let () = unique(voting_CCC_once_per_id(id)) in

        let confirmed_ballot = compose_confirmed_ballot(id, vote_encryption) in
        insert ballot_box(election,ballot_of_confirmed_ballot(confirmed_ballot));
        event InsertedCLEANSED(election,ballot_of_confirmed_ballot(confirmed_ballot));
        get cleansed(=election,=ballot_of_confirmed_ballot(confirmed_ballot)) in
        
        (* send out verification *)
        out(adversary, (id,confirmation_verification_part));
        event VotingPhase_ControlComponentConfirm_Terminates2()
    )
    else
        event VotingPhase_ControlComponentConfirm_Terminates().

let VotingPhase_AgreementProcedure(election:election_id) =
    in(adversary, (=election, id: nat, encryption: encryption_t, vote_signature1: signature_t, vote_signature2: signature_t, confirm_authentication: xor_t));

    get ControlComponent_AuthenticateState(=election,=id,confirm_authentication_hash,confirmation_verification_part) in

    let id_encryption_encoding = encoder_id_encryption_signature_message(election,id, encryption) in
    if (honest_signature_verify(vote_signature1, id_encryption_encoding) && attacker_signature_verify(vote_signature2, id_encryption_encoding) && xor_hash(confirm_authentication) = confirm_authentication_hash) then
    (
        let confirmed_ballot = compose_confirmed_ballot(id, encryption) in
        insert ballot_box(election,ballot_of_confirmed_ballot(confirmed_ballot));

        event VotingPhase_AgreementProcedure_Terminates2()
    ) else (
        event VotingPhase_AgreementProcedure_Terminates()
    ).

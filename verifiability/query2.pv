(* -lib 0_framework/gsverif_library.pvl *)
(* -lib 0_framework/voting_framework.pvl *)
(* -lib 0_framework/properties.pvl *)
(* -lib 1_code_voting/0_primitives.pvl *) 
(* -lib 1_code_voting/0_proverif_utils.pvl *) 
(* -lib 1_code_voting/1_shared.pvl *) 
(* -lib 1_code_voting/2_permutation.pvl *)
(* -lib 1_code_voting/3_setup_phase.pvl *)
(* -lib 1_code_voting/4_voting_phase.pvl *)
(* -lib protocol.pvl *)

(* restrictions *)
restriction v_id:nat, v:vote, e_id:election_id;
  event(Finish(e_id)) && event(HV(e_id,v_id)) ==> event(Verified(e_id,v_id,v))
  [fullSat]
.

(* nounif declations *)
noselect e_id:election_id, x:bitstring; mess(cell_tally( *e_id), *x) / 8000  .
noselect e_id:election_id, x:bitstring; mess(cell_tally( *e_id), *x) / 1 [conclusion].
noselect e_id:election_id, i:nat,x:bitstring; mess(cell_voter( *e_id, *i), *x) / 8000 .
noselect e_id:election_id, i:nat,x:bitstring; mess(cell_voter( *e_id, *i), *x) / 1 [conclusion].
noselect e_id:election_id, x:bitstring; mess(cell_ident( *e_id), *x) / 8000 .
noselect e_id:election_id, x:bitstring; mess(cell_registration( *e_id), *x) /8000 .
noselect e_id:election_id, i:nat,x:bitstring; mess(cell_global_bulletin_board( *e_id), *x) / 8000 .
noselect e_id:election_id, i:nat,x:bitstring; mess(cell_global_bulletin_board( *e_id), *x) / 1 [conclusion].
noselect e_id:election_id, ident:voter_ident,x:bitstring; mess(cell_BB( *e_id, *ident), *x) / 8000 [ignoreAFewTimes].
noselect e_id:election_id, ident:voter_ident,x:bitstring; mess(cell_BB( *e_id, *ident), *x) / 1 [conclusion].
noselect e_id:election_id, i,j:nat, d:data_bulletin_board, d':global_data_bulletin_board; table(bulletin_board( *e_id, *i, *j, *d, *d')) / 8000 .
noselect e_id:election_id, i:nat, v:vote_code_t, ctxt:encryption_t; table(ControlComponent_VoteEncryptionState( *e_id,*i,*v,*ctxt))/1.


(* previously proved properties *)
expand Axioms_GSVerif().

(* proven main lemmas *)
expand Proved_Common().
expand Proved_Cell_BB_HV_HVN().
expand Proved_Cell_BB_Corrupt().
expand Proved_Final_Tally_Voter().
expand Proved_Tally_Decreasing().

(* the query *)
query e_id:election_id, v:vote, v_id,ith_ident:nat, ident:voter_ident, t1,t2:time;
  event(Finish(e_id)) && inj-event(Verified(e_id,v_id,v)) ==>
    inj-event(Counted(e_id,v))@t1 &&

    inj-event(CountedExtended(e_id,v,ith_ident,ident))@t2 &&
    t2 > t1
.

process FinalSystem
